package com.mindera;

import org.apache.commons.lang.StringUtils;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static com.mindera.Config.overwriteStoresMods;

/**
 * Created by MiguelGomes on 13/11/15.
 */
public class PriceBandHelperImpl implements CreatePricesDoc {

    private Map<String, Product> productsMap = new HashMap<String, Product>();

    public void createPrices(Collection<Product> listProducts) {
        try {
            writeToFile(createInsertFromList(listProducts, Config.priceBandId));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String createInsertFromList(Collection<Product> listProducts, int priceBandId) {
        StringBuilder stb = new StringBuilder();

        for(Product parent : listProducts) {
            /*if (!parent.getName().contains("REMOVE")) {
                stb.append(deletePrevious(parent, null, priceBandId));
            }*/
            stb.append(createUpdate(parent, null, priceBandId));
            for(Product son : parent.getChilds()) {
                stb.append(createUpdate(son, parent.getSku(), priceBandId));
            }
            stb.append("\n");
        }

        return stb.toString();
    }

    private String deletePrevious(Product p, String parentSku, int priceBandId) {

        String parent = parentSku == null ? "item_id_parent is null" : "item_id_parent = '" + parentSku + "'";

        String overwriteStoreMod = " price_band_id  = " + priceBandId;
        if (overwriteStoresMods) {
             overwriteStoreMod = " (price_band_id in (select price_band_id from price_band_tbl where parent_price_band_id = " + priceBandId + ") or price_band_id  = " + priceBandId + ")";
        }
        return "delete from price_band_prices_tbl where" + overwriteStoreMod + " and item_id = '" + p.getSku() + "' and category = '" + p.getType().getValue() + "' and " + parent +  "; #" + p.getName() + "\n";

    }

    private String createInsert(Product p, String parentSku, int priceBandId) {

        if(!StringUtils.isEmpty(parentSku)) {
            parentSku = "'" + parentSku + "'";
        }
        System.out.println(p.getName());
        return "insert into price_band_prices_tbl(price_band_id,item_id,item_id_parent,price,updated_by,updated_on,category) VALUES (" + priceBandId + ",'" + p.getSku() + "'," + parentSku
                + "," + p.getExcellPrice() + ",'scriptAuto',CURRENT_TIMESTAMP,'" + p.getType().getValue() + "'); #" + p.getName() + "\n";

    }


    private String createUpdate(Product p, String parentSku, int priceBandId) {
        StringBuilder stringBuilder = new StringBuilder();
        //stringBuilder.append(deletePrevious(p, parentSku, priceBandId));
        stringBuilder.append(deletePrevious(p, parentSku, priceBandId));
        stringBuilder.append(createInsert(p, parentSku, priceBandId));
        return stringBuilder.toString();
    }

    private void writeToFile(String text) {
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(Config.OUTPUT_FILE_PATH), "UTF-8"));
            bw.write(text);
        } catch (Exception e) {
            System.out.println(":(");
        } finally {
            if (bw != null) {
                try {
                    bw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    /*
     * Not important
     */
    private void printProducts(Map<String, Product> mapProducts) {

        for(Product parent : mapProducts.values()) {
            System.out.println(parent.getName() + "(" + parent.getExcellPrice() + ") - " + parent.getType().getValue());
            for(Product son : parent.getChilds()) {
                System.out.println(" - " + son.getName() + "(" + son.getExcellPrice() + ") - " + son.getType().getValue());
            }
        }

    }

}
