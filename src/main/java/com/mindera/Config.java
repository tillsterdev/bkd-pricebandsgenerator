package com.mindera;

/**
 * Created by MiguelGomes on 13/11/15.
 */
public class Config {

    //priceBandId relates to price_band_tbl, pay special attention if the BD has more than 1 tenant.
    public static Integer priceBandId = 3;

    /*
    * ATTENTION - overwriteStoresMods can overwrite price changes set by particular stores.
    * Only set to true, if clean and overwrite prices for all stores is needed.
    */
    public static Boolean overwriteStoresMods = false;

    public static Integer docType = CreatePricesDoc.PRICEBAND;

    public static final String fileName = "AT_Menu_gitter_pommes";


    //in windows - "C:\\Users\\jsrodrigues\\Desktop\\pricebands_NL.sql";
    public static final String OUTPUT_FILE_PATH = "/Users/drk/Downloads/" + fileName + "pricebands.sql";

    public static final String INPUT_FILE_PATH = "/Users/drk/Downloads/" + fileName + ".xlsx";


}
