package com.mindera;

/**
 * Created by MiguelGomes on 8/3/16.
 */
public class CreatePricesDocFactory {

    static CreatePricesDoc getCreatePricesDoc(int type) {
        CreatePricesDoc createPricesDoc;

        switch (type) {
            case CreatePricesDoc.PRICEBAND:
                createPricesDoc = new PriceBandHelperImpl();
                break;
            case CreatePricesDoc.SKUP_MAP:
                createPricesDoc = new SkuMapHelperImpl();
                break;
            default:
                throw new RuntimeException("Invalid type, use CreatePricesDoc.PRICEBAND as example");
        }

        return createPricesDoc;
    }

}
