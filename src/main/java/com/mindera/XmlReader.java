package com.mindera;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by MiguelGomes on 11/21/16.
 */
public class XmlReader implements ReadPrices {

    public static void main(String[] args) throws Exception{
        new XmlReader().getExcellProducts();
    }

    @Override
    public List<Product> getExcellProducts() throws Exception {

        List<Product> productList = new LinkedList<>();

        try {

            File fXmlFile = new File(Config.INPUT_FILE_PATH);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);

            //optional, but recommended
            //read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
            doc.getDocumentElement().normalize();

            NodeList menuItemAgr = doc.getElementsByTagName("MenuItems");

            Element menuItem = (Element) menuItemAgr.item(0);

            //PRODUCTS
            NodeList productsAgr = menuItem.getElementsByTagName("Products");

            NodeList products = ((Element) productsAgr.item(0)).getElementsByTagName("Product");

            for (int temp = 0; temp < products.getLength(); temp++) {

                Node nNode = products.item(temp);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element product = (Element) nNode;

                    productList.add(createProduct(product, ProductTypeEnum.PRODUCT));
                }
            }

            //COMBOS
            NodeList comboAgr = menuItem.getElementsByTagName("Combos");
            NodeList modeAgr = menuItem.getElementsByTagName("Modifiers-Weights");

            NodeList combos = ((Element) comboAgr.item(0)).getElementsByTagName("Combo");

            productList.addAll(getHiearch(combos, "ComboProduct", ProductTypeEnum.COMBO, ProductTypeEnum.COMBO_PRODUCT));

            NodeList mods = ((Element) modeAgr.item(0)).getElementsByTagName("Modifier");

            productList.addAll(getHiearch(mods, "Weight", ProductTypeEnum.MODIFIER, ProductTypeEnum.WEIGHT));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return productList;
    }

    private List<Product> getHiearch(NodeList products, String childTag, ProductTypeEnum parentType, ProductTypeEnum sonType) {

        List<Product> listP = new LinkedList<>();

        for (int comboIndex = 0; comboIndex < products.getLength(); comboIndex++) {

            Element combo = (Element) products.item(comboIndex);

            NodeList comboProducts = combo.getElementsByTagName(childTag);

            List<Product> comboP = new LinkedList<>();

            for (int cpIndex = 0; cpIndex < comboProducts.getLength(); cpIndex++) {
                Element comboProduct = (Element) comboProducts.item(cpIndex);
                comboP.add(createProduct(comboProduct, sonType));
            }

            Product productCombo = createProduct(combo, parentType);
            productCombo.setChilds(comboP);

            listP.add(productCombo);
        }

        return listP;
    }

    private Product createProduct(Element eElement, ProductTypeEnum productTypeEnum) {

        Product p = new Product();
        p.setName(eElement.getAttribute("Name"));
        p.setSku(eElement.getAttribute("Id"));
        try {
            p.setExcellPrice(new BigDecimal(eElement.getAttribute("Price")));
        } catch (Exception e) {
            System.out.println(p.getName());
            p.setExcellPrice(BigDecimal.valueOf(999));
        }
        p.setType(productTypeEnum);

        return p;
    }
}
