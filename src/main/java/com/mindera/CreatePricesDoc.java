package com.mindera;

import java.util.Collection;

/**
 * Created by MiguelGomes on 8/3/16.
 */
public interface CreatePricesDoc {

    int PRICEBAND = 1;
    int SKUP_MAP = 2;

    void createPrices(Collection<Product> listProducts);
}
