package com.mindera;

import java.util.List;

/**
 * Created by MiguelGomes on 13/11/15.
 */
public class Main {
    public static void main(String[] args) throws Exception {

        CreatePricesDoc createPricesDoc = CreatePricesDocFactory.getCreatePricesDoc(Config.docType);

        List<Product> productList = new ExcellHelper().getExcellProducts();

        createPricesDoc.createPrices(productList);
    }
}
