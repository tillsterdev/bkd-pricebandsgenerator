package com.mindera;

import java.util.List;

/**
 * Created by MiguelGomes on 11/21/16.
 */
public interface ReadPrices {

    List<Product> getExcellProducts() throws Exception;
}
