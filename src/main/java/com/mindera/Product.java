package com.mindera;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by MiguelGomes on 13/11/15.
 */
public class Product {

    private ProductTypeEnum type;

    private String name;

    private String sku;

    private String plu;

    private BigDecimal excellPrice;

    private List<Product> childs;

    public ProductTypeEnum getType() {
        return type;
    }

    public void setType(ProductTypeEnum type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getPlu() {
        return plu;
    }

    public void setPlu(String plu) {
        this.plu = plu;
    }

    public List<Product> getChilds() {

        if(childs == null) {
            childs = new LinkedList<Product>();
        }

        return childs;
    }

    public void setChilds(List<Product> childs) {
        this.childs = childs;
    }

    public BigDecimal getExcellPrice() {
        return excellPrice;
    }

    public void setExcellPrice(BigDecimal excellPrice) {
        this.excellPrice = excellPrice;
    }
}
