package com.mindera;

/**
 * Created by MiguelGomes on 13/11/15.
 */
public enum ProductTypeEnum {

    COMBO("COMBO"),
    PRODUCT("PRODUCT"),
    MODIFIER("MODIFIER"),
    WEIGHT("WEIGHT"),
    COMBO_PRODUCT("COMBOPRODUCT"),
    CATEGORY("CATEGORY");

    private String value;

    private ProductTypeEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
