package com.mindera;

/**
 * Created by MiguelGomes on 9/15/16.
 */
public class Test {

        public static boolean areAnagrams(String a, String b) {

            for (char c : a.toCharArray()) {
                int index = b.indexOf(c);
                if (index < 0) {
                    return false;
                }
                b = b.substring(0, index) + b.substring(index + 1, b.length());
            }

            return b.isEmpty();
        }

        public static void main(String[] args) {
            System.out.println(areAnagrams("neurasl", "unreal"));
        }

}
