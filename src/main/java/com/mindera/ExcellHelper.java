package com.mindera;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.util.*;

/**
 * Created by MiguelGomes on 13/11/15.
 */
public class ExcellHelper implements ReadPrices {
    private XSSFWorkbook wb = null;

    private static final int PARENT = 0;
    private static final int SON = 2;
    private static final int PRICE = 4;
    private static final int SKU = 5;
    private static final int PLU = 6;

    public List<Product> getExcellProducts() throws Exception {

        FileInputStream fin = new FileInputStream(new File(Config.INPUT_FILE_PATH));

        wb = new XSSFWorkbook(fin);

        int numberOfSheets = wb.getNumberOfSheets();

        List<Product> listProds = new LinkedList<>();

        for (int i = 0; i < numberOfSheets; i++) {
            System.out.println("[Label: " + wb.getSheetAt(i).getSheetName()+"]");
            listProds.addAll(readSheet(wb.getSheetAt(i)));
        }

        printProducts(listProds);

        return listProds;
    }

    private void printProducts(List<Product> listProducts) {

        for (Product parent : listProducts) {
            System.out.println(parent.getName() + "(" + parent.getExcellPrice() + ")");
            for (Product son : parent.getChilds()) {
                System.out.println(" - " + son.getName() + "(" + son.getExcellPrice() + ")");
            }
        }

    }

    private List<Product> readSheet(XSSFSheet sheet) {

        Iterator<Row> rowIt = sheet.iterator();

        String sheetName = sheet.getSheetName();

        //Trash
        if (rowIt.hasNext()) {
            rowIt.next();
        }

        List<Product> listProds = new LinkedList<Product>();

        Product parent = null;
        Product product = null;

        while (rowIt.hasNext()) {
            Row row = rowIt.next();

            System.out.println( "[rowNumber " + row.getRowNum()+"]");

            if (isValidRow(row)) {
                if (hasStringValue(row.getCell(PARENT))) {
                    parent = constructProduct(row);
                    addProductType(sheetName, null, parent);
                    listProds.add(parent);
                } else if (hasStringValue(row.getCell(SON))) {
                    product = constructProduct(row);
                    addProductType(sheetName, product, parent);
                    parent.getChilds().add(product);
                }
            }

        }

        return listProds;
    }

    private void addProductType(String sheetName, Product product, Product parentProduct) {

        if (isMod(sheetName)) {
            parentProduct.setType(ProductTypeEnum.MODIFIER);
            if(product != null) {
                product.setType(ProductTypeEnum.WEIGHT);
            }
        } else {
            if(product == null) {
                parentProduct.setType(ProductTypeEnum.PRODUCT);
            } else {
                parentProduct.setType(ProductTypeEnum.COMBO);
                product.setType(ProductTypeEnum.COMBO_PRODUCT);
            }
        }
    }

    private boolean isMod(String name) {
        return name.equalsIgnoreCase("MODIFIERS") || name.startsWith("MODIFIER") || name.startsWith("MODIFIER2");
    }

    private boolean hasStringValue(Cell cellDecide) {
        return cellDecide != null && cellDecide.getCellType() == Cell.CELL_TYPE_STRING && StringUtils.isNotBlank(cellDecide.getStringCellValue());
    }

    private String tryParse(Cell c) {
        try {
            return c.getStringCellValue();
        } catch (Exception e) {
            return c.getNumericCellValue() + "";
        }
    }

    private boolean isValidRow(Row row) {
        Cell PRICE_CELL = row.getCell(PRICE);
        Cell SKU_CELL = row.getCell(SKU);
        if (PRICE_CELL != null && SKU_CELL != null) {
            String PRICE_VALUE = tryParse(PRICE_CELL);
            String SKU_VALUE = tryParse(PRICE_CELL);
            if (StringUtils.isNotEmpty(PRICE_VALUE)) System.out.println("PRICE VALUE MISSING");
            if (StringUtils.isNotEmpty(SKU_VALUE)) System.out.println("SKU VALUE MISSING");
            return StringUtils.isNotEmpty(PRICE_VALUE) && StringUtils.isNotEmpty(SKU_VALUE);
        } else {
            if (PRICE_CELL == null) System.out.println("MISSING PRICE CELL");
            if (SKU_CELL == null) System.out.println("MISSING PRICE CELL");
            return false;
        }
    }

    private Product constructProduct(Row row) {

        Product product = new Product();

        product.setName(getProductName(row));
        product.setExcellPrice(getPrice(row.getCell(PRICE)));
        String skuValue = row.getCell(SKU).getStringCellValue();
        if (skuValue.toCharArray()[0] == '\ufeff') {
            // Was BOMed in UTF-16.
            // we can splice this char which as only meaning for text editors which don't default to UTF-8
            // probably caused by using files created on windows in older versions of Excell
            skuValue = skuValue.substring(1);
        }
        product.setSku(skuValue);
        if (row.getCell(PLU) != null) {
            product.setPlu(getValue(row.getCell(PLU)));
        }

        return product;

    }

    private String getValue(Cell cell) {
        if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
            return cell.getStringCellValue();
        } else if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
            return Long.toString((long)cell.getNumericCellValue());
        } else {
            return "";
        }
    }

    private String getProductName(Row row) {

        if (hasStringValue(row.getCell(PARENT))) {
            return row.getCell(PARENT).getStringCellValue();
        } else if (hasStringValue(row.getCell(SON))) {
            return row.getCell(SON).getStringCellValue();
        }

        return StringUtils.EMPTY;

    }

    private BigDecimal getPrice(Cell cell) {

        BigDecimal returnValue = BigDecimal.ZERO;

        try {
            returnValue = new BigDecimal(cell.getStringCellValue());
        } catch (Exception e) {
            System.out.println("hm....");
        }

        return returnValue;
    }
}
