package com.mindera;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by MiguelGomes on 8/3/16.
 */
public class SkuMapHelperImpl implements CreatePricesDoc {
    public void createPrices(Collection<Product> listProducts) {

        List<Product> productOnlyList = new ArrayList<Product>();

        long withPlu = 0;
        long withoutPlu = 0;

        for (Product p : listProducts) {
            //if (p.getType() == ProductTypeEnum.PRODUCT) {
                productOnlyList.add(p);
                if (p.getPlu() != null && !p.getPlu().equals("")) {
                    withPlu++;
                    System.out.println("<apply-template emn8Sku='" + p.getSku() + "' posSku='" + p.getPlu() + "' displayName='" + p.getName() + "'/>");
                } else {
                    withoutPlu++;
                }
            //}
        }

        System.out.println("Status");
        System.out.println("With Plu:" + withPlu);
        System.out.println("Without Plu" + withoutPlu);

    }
}
